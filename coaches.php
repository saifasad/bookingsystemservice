<?
	//Connect to the remote database
	$conn = mysqli_connect("db4free.net", "sdp2016user", "sdp2016pw", "sdp2016database");
	//Prepare a query to retrieve coaches and their IDs
	$query = "SELECT user_id, user_name FROM users WHERE user_iscoach='y'";
	$count = 0;
	//Execute the query
	$result = mysqli_query($conn, $query);
	//Declare an array to send all coaches back to the app (through JSON)
	$array = array();
	while ($row = mysqli_fetch_assoc($result)) {
		//Declare a map array to represent one coach
		$subarray = array();
		$subarray['coach_id'] = $row['user_id'];
		$subarray['name'] = $row['user_name'];
		$array[$count] = $subarray;
		$count = $count + 1;
	}
	//Send the array back to the app as a JSON object
	echo json_encode($array);
?>	