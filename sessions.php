<?
	//Retrieve the coach id and date from the app
	$coach_id = $_GET['coach_id'];
	$date = $_GET['date'];
	//Connect to the remote database
	$conn = mysqli_connect("db4free.net", "sdp2016user", "sdp2016pw", "sdp2016database");
	//Prepare a query to retrieve sessions
	$query = "SELECT * FROM session WHERE coach_id='$coach_id' AND date='$date'";
	$count = 0;
	//Execute the query
	$result = mysqli_query($conn, $query);
	//Prepare an array to store all sessions (for JSON encoding to the server)
	$array = array();
	while ($row = mysqli_fetch_assoc($result)) {
		//Declare a map array to represent a single session
		$subarray = array();
		$subarray['time'] = $row['start_time']."-".$row['finish_time'];
		$subarray['date'] = $row['date'];
		$subarray['session'] = $row['session_id'];
		$array[$count] = $subarray;
		$count = $count + 1;
	}
	//Encode the array to send all sessions back to the app
	echo json_encode($array);
?>	